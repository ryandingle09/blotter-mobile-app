import store from './assets/vuex/storage'

export default [
  {
    path: '/',
    redirect: function (route, resolve, reject) {
      if (store.state.user.username) {
        console.log('Detected as login');
        resolve('/home');
      }else{
          console.log('PLease Login');
          resolve('/login');
      }
    },
  },
  {
    path: '/home',
    component: require('./assets/vue/pages/complaint/home.vue')
  },
  {
    path: '/hearing',
    component: require('./assets/vue/pages/hearing/hearing.vue')
  },
  {
    path: '/account',
    component: require('./assets/vue/pages/account/account.vue')
  },
  {
    path: '/login',
    component: require('./assets/vue/pages/auth/login.vue')
  },
  {
    path: '/register',
    component: require('./assets/vue/pages/auth/register.vue')
  },
  {
    path: '/add',
    component: require('./assets/vue/pages/complaint/add.vue')
  },
  {
    path: '/edit/:id/',
    component: require('./assets/vue/pages/complaint/edit.vue')
  },
  {
    path: '/show/:id/',
    component: require('./assets/vue/pages/complaint/show.vue')
  },
  {
    path: '/panel-left/',
    component: require('./assets/vue/pages/panel-left.vue')
  },
  {
    path: '/about/',
    component: require('./assets/vue/pages/about.vue')
  },
  {
    path: '/form/',
    component: require('./assets/vue/pages/form.vue')
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: require('./assets/vue/pages/dynamic-route.vue')
  },
  {
    path: '/color-themes/',
    component: require('./assets/vue/pages/color-themes.vue')
  },
  {
    path: '/chat/',
    component: require('./assets/vue/pages/chat.vue')
  },
  {
    path: '/vuex/',
    component: require('./assets/vue/pages/vuex.vue')
  },
]
